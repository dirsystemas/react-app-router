window._users = {
    data:[
    {
        userName: "tueres", 
        pass: "123",   
        firstName:"Paco",
        lastName: "Herber",
        email: "tueres@gmail.com",
        rol: "Administrador",
        area : "Administracion"
    },
    {
        userName: "yomero", 
        pass: "123",   
        firstName:"Hugo",
        lastName: "Patiño",
        email: "yomero@gmail.com",
        rol: "Usuario",
        area : "Contabilidad"
    },
    {
        userName: "luisito", 
        pass: "123",  
        firstName:"Luis",
        lastName: "Omaña",
        email: "luisito@gmail.com",
        rol: "Usuario",
        area : "Medio Ambiente"
    }

]};

window._personal = {
    name: 'Noemi Leon',
    especialidad: 'Desarrollo de Software',
    habilidades: 'Desarollo de aplicaciones de escritorio, web y extensiones de navegacion. Tecnologias de la informacion',
    email: 'noemi.leon@desarrollo.org',
    fecha_act: '21 Diciembre, 1990',
    mobil: '771 71 48 9139',
    direccion: 'Mexico',
    photo_url: 'img/silueta.jpg',
    skillsDevelop: [
        {
            title: 'React.js',
            width: '98',
            widthPerc: '98%',
            type: 1
        },
        {
            title: 'Node.js',
            width: '90',
            widthPerc: '90%',
            type: 1
        },
        {
            title: 'Python',
            width: '15',
            widthPerc: '15%',
            type: 1
        },
        {
            title: 'Java',
            width: '85',
            widthPerc: '85%',
            type: 1
        },
   
        {
            title: 'Git',
            width: '60',
            widthPerc: '60%',
            type: 2
        },
        {
            title: 'MongoDB',
            width: '40',
            widthPerc: '40%',
            type: 2
        },
        {
            title: 'Ingles',
            width: '75',
            widthPerc: '75%',
            type: 2
        }
    ],
    portfolio:[
        {
            icon: 'fas fa-credit-card',
            title: 'Sistema de pagos',
            descripcion: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Accusantium voluptatem sint dolorum maxime soluta. Eveniet iusto animi deleniti, dolorem magnam ab ex repellat quam atque quia iste voluptatum vitae quod?',
            link: '#'
        },
        {
            icon: 'fas fa-anchor',
            title: 'Aplicaciones de registro',
            descripcion: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Accusantium voluptatem sint dolorum maxime soluta. Eveniet iusto animi deleniti, dolorem magnam ab ex repellat quam atque quia iste voluptatum vitae quod?',
            link: '#'
        },
        {
            icon: 'fas fa-file-video',
            title: 'Descarga de videos',
            descripcion: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Accusantium voluptatem sint dolorum maxime soluta. Eveniet iusto animi deleniti, dolorem magnam ab ex repellat quam atque quia iste voluptatum vitae quod?',
            link: '#'
        },
        {
            icon: 'fas fa-camera',
            title: 'Captura de pantalla',
            descripcion: 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Accusantium voluptatem sint dolorum maxime soluta. Eveniet iusto animi deleniti, dolorem magnam ab ex repellat quam atque quia iste voluptatum vitae quod?',
            link: '#'
        }
    ]
}


window.items =  [
    {
    "id": 1,
    "name": "Leanne Graham",
    "username": "Bret",
    "email": "Sincere@april.biz",
    "address": {
    "street": "Kulas Light",
    "suite": "Apt. 556",
    "city": "Gwenborough",
    "zipcode": "92998-3874",
    "geo": {
    "lat": "-37.3159",
    "lng": "81.1496"
    }
    },
    "phone": "1-770-736-8031 x56442",
    "website": "hildegard.org",
    "company": {
    "name": "Romaguera-Crona",
    "catchPhrase": "Multi-layered client-server neural-net",
    "bs": "harness real-time e-markets"
    }
    },
    {
    "id": 2,
    "name": "Ervin Howell",
    "username": "Antonette",
    "email": "Shanna@melissa.tv",
    "address": {
    "street": "Victor Plains",
    "suite": "Suite 879",
    "city": "Wisokyburgh",
    "zipcode": "90566-7771",
    "geo": {
    "lat": "-43.9509",
    "lng": "-34.4618"
    }
    },
    "phone": "010-692-6593 x09125",
    "website": "anastasia.net",
    "company": {
    "name": "Deckow-Crist",
    "catchPhrase": "Proactive didactic contingency",
    "bs": "synergize scalable supply-chains"
    }
    },
    {
    "id": 3,
    "name": "Clementine Bauch",
    "username": "Samantha",
    "email": "Nathan@yesenia.net",
    "address": {
    "street": "Douglas Extension",
    "suite": "Suite 847",
    "city": "McKenziehaven",
    "zipcode": "59590-4157",
    "geo": {
    "lat": "-68.6102",
    "lng": "-47.0653"
    }
    },
    "phone": "1-463-123-4447",
    "website": "ramiro.info",
    "company": {
    "name": "Romaguera-Jacobson",
    "catchPhrase": "Face to face bifurcated interface",
    "bs": "e-enable strategic applications"
    }
    },
    {
    "id": 4,
    "name": "Patricia Lebsack",
    "username": "Karianne",
    "email": "Julianne.OConner@kory.org",
    "address": {
    "street": "Hoeger Mall",
    "suite": "Apt. 692",
    "city": "South Elvis",
    "zipcode": "53919-4257",
    "geo": {
    "lat": "29.4572",
    "lng": "-164.2990"
    }
    },
    "phone": "493-170-9623 x156",
    "website": "kale.biz",
    "company": {
    "name": "Robel-Corkery",
    "catchPhrase": "Multi-tiered zero tolerance productivity",
    "bs": "transition cutting-edge web services"
    }
    },
    {
    "id": 5,
    "name": "Chelsey Dietrich",
    "username": "Kamren",
    "email": "Lucio_Hettinger@annie.ca",
    "address": {
    "street": "Skiles Walks",
    "suite": "Suite 351",
    "city": "Roscoeview",
    "zipcode": "33263",
    "geo": {
    "lat": "-31.8129",
    "lng": "62.5342"
    }
    },
    "phone": "(254)954-1289",
    "website": "demarco.info",
    "company": {
    "name": "Keebler LLC",
    "catchPhrase": "User-centric fault-tolerant solution",
    "bs": "revolutionize end-to-end systems"
    }
    }
]