var arrSteps = ['paso1', 'paso2', 'paso3', 'paso4'];
var cont = 0;
function stepProgres() {
	remove();
	if(cont>0){
		addCompleted(cont);
	}
	
   var element = document.getElementById(arrSteps[cont]);
   element.classList.add("active");
   document.getElementById(arrSteps[cont]+'_desc').innerHTML = "(active)";
   cont ++; 
}

function addCompleted(item) {
  for(var index = 0; index < item; index++){
	var element = document.getElementById(arrSteps[index]);
	element.classList.add("completed");
	document.getElementById(arrSteps[index]+'_desc').innerHTML = "(complete)";
  }
}

function remove() {
  for(var index = 0; index < arrSteps.length; index++){
	var element = document.getElementById(arrSteps[index]);
	element.classList.remove('completed', 'active');
	document.getElementById(arrSteps[index]+'_desc').innerHTML = "";
  }
  
}

function reset() {
  remove()
  cont = 0;
}