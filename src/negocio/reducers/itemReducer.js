import {GET_ITEMS, ADD_ITEM, DELETE_ITEM} from '../actions/types';
const initialState = {
    items: [] //window.items
    
}


export default function(state=initialState, action){
    switch(action.type){
        case 'INIT_ITEMS':
            return {
                ...state,
                items: [ ...state.items, ...action.items] //state.items.concat(action.items) 
            }

        case GET_ITEMS:
            return{
                ...state
            }
        case  ADD_ITEM:
            return{
                ...state,
                items: [action.payload, ...state.items]
            }    
        case  DELETE_ITEM:
            return{
                ...state,
                items: state.items.filter(item => item.id !== action.payload)
            }
        default:
            return state    
    }
}