import { combineReducers } from 'redux';
import itemReducer from './reducers/itemReducer';
import { createStore } from 'redux';

export default combineReducers({
    itemState: itemReducer
});

//export default createStore()