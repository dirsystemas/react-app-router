import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Login from './Pages/Login/Login';
import UserForm from './Pages/Registro/UserForm';
import Main from './Pages/Main/Main';
import MongoDB from './Pages/MongoDB/MongoDB';
import Records from './Pages/Records/Records';
import NoAuthority from './Pages/NoAuthority';
import PageNotFound from './Pages/components/PageNotFound';


const Navigator = () => (
    <Router>
        <Switch>
            <Route path='/' component={Login} exact />
           
            <Route path='/main' component={Main} />
            <Route path='/registro' component={UserForm} />
            
            <Route path='/mongodb' component={MongoDB} />
            <Route path='/records' component={Records} />
            <Route path="/noAuthority" component={NoAuthority}/>
            <Route path='*' component={PageNotFound}/>
        </Switch>
      
    </Router>
)

export default Navigator;