import React, { Component } from 'react';

import MainInfo from './Pages/Main/MainInfo';
import Skills from './Pages/Main/Skills';
import Portfolio from './Pages/Main/Portfolio';

class App extends Component {
  render() {
    return (
        <div>
        
            <section className='main-info bg-grad' id='main-info'>
                <MainInfo />
            </section>
            <section className='sect skills' id='skills'>
                <Skills />
            </section>
            <section className='sect portfolio bg-grad-b' id='portfolio'>
                <Portfolio />
            </section>
        </div>
  );
  }
}

export default App;
