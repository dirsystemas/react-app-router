import React, { Component } from 'react';
import { Redirect } from 'react-router-dom'

class Login extends Component {
  state = {
      redirectToReferrer: false
  }

  _onLogin = () =>{
    this.setState(
      {redirectToReferrer: true}
    )
   
  }

  render() {

    if (this.state.redirectToReferrer) {
      return (<Redirect to={'/main'}/>)
    }
    
    return (
          <div style={{paddingTop: '3rem'}}>
              
              <form className="form-signin" style={{width: '500px'}}>
                  <div className="text-center mb-4">
                      <img className="mb-4" src="img/usuario.png" alt="" style={{padding:'15px',maxWidth:'35%', border:'6px solid #444747', borderRadius:'50%'}} />
                      <h1 className="h3 mb-3 font-weight-normal">Autentificación</h1>
                     
                  </div>

                  <div className="form-label-group">
                    <input type="email" id="inputEmail" className="form-control" placeholder="Ingrese nombre de usuario"  autoFocus />
                    <label htmlFor="inputEmail">Usuario</label>
                  </div>

                  <div className="form-label-group">
                    <input type="password" id="inputPassword" className="form-control" placeholder="Ingrese su contraseña"  />
                    <label htmlFor="inputPassword">Contraseña</label>
                  </div>

                 
                  <button 
                    className="btn btn-lg btn-primary btn-block" 
                    type="button"
                    onClick={this._onLogin}
                  >Aceptar
                  </button>
                  <button 
                    className="btn btn-lg btn-secondary btn-block" 
                    type="reset"
                  >Reset
                  </button>
                 
                  <p className="mt-5 mb-3 text-muted text-center">&copy; Curso React - Node 2019</p>
              </form>
          </div>
      
    );
  }
}

export default Login;