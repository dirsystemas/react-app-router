import React, { Component } from 'react';

class Row extends Component{

    render(){
        const { name, username, email, phone } = this.props

        return(
           
                <tr>
                    <td>{name}</td>
                    <td>{username}</td>
                    <td>{email}</td>
                    <td>{phone}</td>
                 </tr>
          
        )
    }
}

export default Row;