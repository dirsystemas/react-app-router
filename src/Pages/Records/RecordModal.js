import React, { Component } from 'react';

class RecordModal extends Component{

    onClick = e => {
        document.getElementById('name').value= ''
        document.getElementById('username').value= ''
        document.getElementById('email').value= ''
        document.getElementById('phone').value= ''
        this.props.onClick(e)
    }

    render(){
        return(
            <div>
                <button type="button" className="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">Insertar registro</button>

                <div className="modal fade" id="exampleModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLabel">Nuevo registro</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <form >
                                <div className="form-group">
                                    <label htmlFor="name" className="col-form-label">Nombre:</label>
                                    <input 
                                        type="text" 
                                        className="form-control" 
                                        id="name" 
                                        onChange={this.props.onChange} 
                                    />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="username" className="col-form-label">Usuario:</label>
                                    <input 
                                        type="text" 
                                        className="form-control" 
                                        id="username" 
                                        onChange={this.props.onChangeUsername}
                                    />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="email" className="col-form-label">Email:</label>
                                    <input 
                                        type="text" 
                                        className="form-control" 
                                        id="email" 
                                        onChange={this.props.onChangeEmail}
                                        />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="phone" className="col-form-label">Telefono:</label>
                                    <input 
                                        type="text" 
                                        className="form-control" 
                                        id="phone" 
                                        onChange={this.props.onChangePhone}
                                        />
                                </div>
                              
                            </form>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                            <button 
                                type="button" 
                                className="btn btn-primary"
                                data-dismiss="modal"
                                onClick={this.onClick}
                                >Aceptar</button>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default RecordModal;