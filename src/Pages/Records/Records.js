import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getItems, deleteItem } from '../../negocio/actions/itemActions';

import axios from 'axios';
import NavbarInverse from '../components/NavbarInverse';
import Row from './Row';
import RecordModal from './RecordModal';

class Records extends Component {
    state = {
        users: [],
       
        name: '', 
        username: '', 
        email: '', 
        phone:''
        
    }

    componentDidMount(){
        this.props.getItems()
    }
   /*
    componentDidMount(){
        axios('https://jsonplaceholder.typicode.com/users')
            .then(res =>{
                this.setState({
                    users: res.data
                })
                console.log(this.state.users)
            })
    }
    */

    onChange = e => {
        this.setState({
            name: e.target.value
        })
       
    }

    onChangeUsername = e => {
        this.setState({
            username: e.target.value
        })
       
    }

    onChangeEmail = e => {
        this.setState({
            email: e.target.value
        })
       
    }

    onChangePhone = e => {
        this.setState({
            phone: e.target.value
        })
       
    }

    onClick = e => {
    
        this.setState(
            { users: [  ...this.state.users, {
                name: this.state.name, 
                username: this.state.username, 
                email: this.state.email, 
                phone:this.state.phone
                }, 
               
            ]}
        )
      

    }

    render() {
        const { items } = this.props.items
       
        return (
            <div>
                <NavbarInverse 
                    itemRecords='disabled'
                />
                <div  style={{ marginTop:'100px'}}>
                    
                    <div className="container" style={{minWidth:'500px'}}>
                    <RecordModal 
                        onChange={this.onChange}
                        onChangeUsername={this.onChangeUsername}
                        onChangeEmail={this.onChangeEmail}
                        onChangePhone={this.onChangePhone}
                        onClick={this.onClick}
                        
                    />
                        <h2>Registro de Clientes</h2>
                                
                        <table className="table table-striped">
                            <thead>
                                <tr>
                                <th>Nombre</th>
                                <th>Usuario</th>
                                <th>Email</th>
                                <th>Telefono</th>
                                </tr>
                            </thead>
                            <tbody>
                            
                            {items.map( ( item ) =>(
                                <tr key={item.id}>
                                    <td>{item.name}</td>
                                    <td>{item.username}</td> 
                                    <td>{item.email}</td>
                                    <td>{item.phone}</td> 
                                </tr>
                            ) )}

                           
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        
        );
    }

    _renderRows = (arr) => {
        return arr.map( (item, i) => {
            return (
                <Row {...item} key= {i}/>
            );
        });
    };
}

const mapStateToProps = state => ({
    items: state.itemState
})

export default connect(
    mapStateToProps, 
    {getItems, deleteItem}
    )(Records);