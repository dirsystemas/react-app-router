import React, { Component } from 'react';


class FormUserDetails extends Component{
    handleSubmit = e => {
     //  e.preventDefault()
       this.props.nextStep()

   }  

   render(){
        const { values, handleChange } = this.props
        return (
            <div>
                <h4>Ingresar Informacion del Usuario</h4>
                <form onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <label htmlFor="userName">Usuario:</label>
                        <input 
                            type="text" 
                            className="form-control" 
                            id="userName" 
                            onChange={handleChange('userName')}
                            defaultValue={values.userName} 
                            placeholder="Ingrese un Usuario"
                            required="required" 
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="email">Email:</label>
                        <input 
                            type="email" 
                            className="form-control" 
                            id="email" 
                            onChange={handleChange('email')}
                            defaultValue={values.email} 
                            placeholder="Ingrese su Email"
                            required="required" 
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="pass">Contraseña:</label>
                        <input 
                            type="password" 
                            className="form-control" 
                            id="pass" 
                            onChange={handleChange('pass')}
                            defaultValue={values.pass} 
                            placeholder="Ingrese su contraseña"
                            required="required" 
                        />
                    </div>
                    <div className="form-group">
                        <button 
                            className="btn btn-lg btn-primary btn-block" 
                            type="submit"
                           
                        >Continuar
                        </button>
                    </div>
                
                </form>
     
            </div>
        )
   }
}

export default FormUserDetails;