import React, { Component } from 'react';
import { Redirect } from 'react-router-dom'

class FormSuccess extends Component{

  state = {
    redirectToRegistro: false
  }

   render(){
    if (this.state.redirectToRegistro) {
        return (<Redirect to={'/main'}/>)
    }  
        
        return (
            <div>
                <h4>Gracias por su ingreso</h4>
                <br/>
                <button 
                    className="btn btn-lg btn-primary btn-block" 
                    type="button"
                    style= {_styles.button}
                    onClick= {
                        () => {
                            this.setState(
                                {redirectToRegistro: true}
                            )
                        }
                    }
                >Ingresar
                </button>
                                              
            </div>
        )
   }
}

const _styles = {
    button:{
        margin: 15
    }
}
export default FormSuccess;