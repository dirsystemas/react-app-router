import React, { Component } from 'react';
import FormUserDetails from './FormUserDetails';
import FormPersonalDetails from './FormPersonalDetails';
import FormConfirm from './FormConfirm';
import FormSuccess from './FormSuccess';
import AppbarMult from '../components/AppbarMult';
import StepProgresbar from './StepProgresbar';

class UserForm extends Component {
    state = {  
        step: 0,
        userName:'',
        firstName: '',
        lastName: '',
        email: '',
        rol: '',
        area: '',
        pass: '',
        stepCompleted_1: '',
        stepCompleted_2: ''
    }

    
    completedStep = () => {
        if(this.state.userName && this.state.email && this.state.pass)
            this.setState({
                stepCompleted_1: 'Completada'
            })
        else
            this.setState({
                stepCompleted_1: ''
            })

        if(this.state.firstName && this.state.lastName && this.state.rol && this.state.area)
            this.setState({
                stepCompleted_2: 'Completada'
            })
        else
            this.setState({
                stepCompleted_2: ''
            })    
    }

    nextStep = () => {
        const { step } = this.state 
        this.setState({
            step: step + 1
        })
        this.completedStep();
    }

    prevStep = () => {
        const { step } = this.state 
        this.setState({
            step: step - 1
        })
        this.completedStep();
    }

    handleChange = input => e => {
        this.setState( { [input]: e.target.value } )
    }

    render() { 
        const { step } = this.state
        const {userName, firstName, lastName, email, rol, area, pass} = this.state
        const values = {userName, firstName, lastName, email, rol, area, pass} 
      
        // eslint-disable-next-line default-case

        switch(step){
            case 0:
                return(
                    <div>
                        <AppbarMult title = 'Ingresar Detalles del Usuario'/>
                        <div className='container-step-bar'>
                            <StepProgresbar 
                                classNameStep1= 'active'
                                stepCompleted_1= {this.state.stepCompleted_1}
                                stepCompleted_2= {this.state.stepCompleted_2}
                            />
                        </div>  
                        <div className='div-registro'>  
                            <FormUserDetails
                                nextStep= {this.nextStep}
                                handleChange = {this.handleChange}
                                values = {values}
                            /> 
                        </div>
                        
                   </div>
                )
            case 1: 
                return(
                    <div>
                        <AppbarMult title = 'Ingresar Detalles Personales'/>
                        <div className='container-step-bar'>
                            <StepProgresbar 
                                classNameStep1= 'completed'
                                classNameStep2= 'active'
                                stepCompleted_1= {this.state.stepCompleted_1}
                                stepCompleted_2= {this.state.stepCompleted_2}
                            />
                        </div>  
                        <div className='div-registro'>  
                            <FormPersonalDetails
                                nextStep= {this.nextStep}
                                prevStep= {this.prevStep}
                                handleChange = {this.handleChange}
                                values = {values}
                            /> 
                        </div>
                   </div>
                )    
            case 2: 
                return(
                    <div>
                        <AppbarMult title = 'Confirmar Informacion del Usuario'/>
                        <div className='container-step-bar'>
                            <StepProgresbar 
                                classNameStep1= 'completed'
                                classNameStep2= 'completed'
                                classNameStep3= 'active'
                                stepCompleted_1= {this.state.stepCompleted_1}
                                stepCompleted_2= {this.state.stepCompleted_2}
                            />
                        </div>
                        <div className='div-registro'>  
                            <FormConfirm
                                nextStep= {this.nextStep}
                                prevStep= {this.prevStep}
                                values = {values}
                            />
                        </div>
                    </div> 
                )     
            case 3: 
                return(
                    <div>
                        <AppbarMult title = 'Gracias por su ingreso'/>
                        <div className='container-step-bar'>
                            <StepProgresbar 
                                classNameStep1= 'completed'
                                classNameStep2= 'completed'
                                classNameStep3= 'completed'
                                classNameStep4= 'active'
                                stepCompleted_1= {this.state.stepCompleted_1}
                                stepCompleted_2= {this.state.stepCompleted_2}
                            />
                        </div> 
                        <div className='div-registro'>  
                            <FormSuccess/>
                        </div>    
                    </div> 
                )  
             default:
                return(
                    <label>Estoy en default </label>
                )        
        }
    
   
    }
}


 
export default UserForm;