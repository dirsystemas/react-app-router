import React from 'react';


export default function StepProgresbar(props){
    return(
        <ul className="progress-indicator">
            <li className={props.classNameStep1}>
                <span className="bubble"></span>
                Informacion usuario. <br/>
                <small>
                    {props.stepCompleted_1}
                </small>
            </li>
            <li className={props.classNameStep2} >
                <span className="bubble"></span>
                Informacion personal. <br/>
                <small >
                    {props.stepCompleted_2}
                </small>
            </li>
            <li className={props.classNameStep3} >
                <span className="bubble"></span>
                Confirmar. <br/><small ></small>
            </li>
            <li className={props.classNameStep4} >
                <span className="bubble"></span>
                Terminado.<br/><small ></small>
            </li>
          
        </ul>	
    )
}