import React from 'react';

export default function BarStepProgress(props){
    return(
        <div className='container-step-bar' style={{width:'500px'}}>
            <ul className="progress-indicator">
                <li id='paso1'>
                    <span className="bubble"></span>
                    Inf. usuario. <br/><small id='paso1_desc'></small>
                </li>
                <li id='paso2'>
                    <span className="bubble"></span>
                    Inf. personal. <br/><small id='paso2_desc'></small>
                </li>
                <li id='paso3'>
                    <span className="bubble"></span>
                    Confirmar. <br/><small id='paso3_desc'></small>
                </li>
                <li id='paso4'>
                    <span className="bubble"></span>
                    Terminado.<br/><small id='paso4_desc'></small>
                </li>
            
            </ul>	
        </div>
    )
}