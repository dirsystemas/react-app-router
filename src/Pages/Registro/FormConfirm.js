import React, { Component } from 'react';


class FormConfirm extends Component{
   _continue = e => {
       e.preventDefault()
       //process form
       this.props.nextStep()

   } 

   _back = e => {
    e.preventDefault()
    this.props.prevStep()

 }  

   render(){
        const {  values: {userName, firstName, lastName, email, rol, area} } = this.props
        return (
            <div>
                <h4>Confirmar Informacion del Usuario</h4>
                <div className="list-group">
                    <a href="#" className="list-group-item list-group-item-action flex-column align-items-start">
                        <div className="d-flex w-100 justify-content-between">
                        <h5 className="mb-1">Usuario:</h5>
                        </div>
                        <p className="mb-1">{userName}</p>
                    </a>
                    <a href="#" className="list-group-item list-group-item-action flex-column align-items-start">
                        <div className="d-flex w-100 justify-content-between">
                        <h5 className="mb-1">Email:</h5>
                        </div>
                        <p className="mb-1">{email}</p>
                    </a>
                    <a href="#" className="list-group-item list-group-item-action flex-column align-items-start">
                        <div className="d-flex w-100 justify-content-between">
                        <h5 className="mb-1">Nombre(s):</h5>
                        </div>
                        <p className="mb-1">{firstName}</p>
                    </a>
                    
                    <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
                        <div className="d-flex w-100 justify-content-between">
                        <h5 className="mb-1">Apellido(s):</h5>
                        </div>
                        <p className="mb-1">{lastName}</p>
                    </a>
                    <a href="#" className="list-group-item list-group-item-action flex-column align-items-start">
                        <div className="d-flex w-100 justify-content-between">
                        <h5 className="mb-1">Rol:</h5>
                        </div>
                        <p className="mb-1">{rol}</p>
                    </a>
                    <a href="#" className="list-group-item list-group-item-action flex-column align-items-start">
                        <div className="d-flex w-100 justify-content-between">
                        <h5 className="mb-1">Area:</h5>
                        </div>
                        <p className="mb-1">{area}</p>
                    </a>            
                </div>        
                   
                <br/>
                <button 
                    className="btn btn-lg btn-primary btn-block" 
                    type="button"
                  
                    onClick={this._continue}
                >Confirmar & Continuar
                </button>
                <button 
                    className="btn btn-lg btn-secondary btn-block" 
                    type="button"
               
                    onClick={this._back}
                >Regresar
                </button>        
          
                               
            </div>
        )
   }
}


export default FormConfirm;