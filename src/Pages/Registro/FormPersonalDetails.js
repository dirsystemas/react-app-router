import React, { Component } from 'react';


class FormPersonalDetails extends Component{
   _continue = e => {
       e.preventDefault()
       this.props.nextStep()

   } 

   _back = e => {
    e.preventDefault()
    this.props.prevStep()

 }  

   render(){
        const { values, handleChange } = this.props
        return (
            <div>
                <h4>Ingresar Informacion Personal del Usuario</h4>
                <form>
                    <div className="form-group">
                        <label htmlFor="firstName">Nombre(s):</label>
                        <input 
                            type="text" 
                            className="form-control" 
                            id="firstName" 
                            onChange={handleChange('firstName')}
                            defaultValue={values.firstName} 
                            placeholder="Ingrese Nombre(s)"
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="lastName">Apellido(s):</label>
                        <input 
                            type="text" 
                            className="form-control" 
                            id="lastName" 
                            onChange={handleChange('lastName')}
                            defaultValue={values.lastName} 
                            placeholder="Ingrese Apellido(s)"
                        />
                    </div>
                    <div class="form-group">
                        <label htmlFor="rol">Rol:</label>
                        <input 
                            type="text" 
                            className="form-control" 
                            id="rol" 
                            onChange={handleChange('rol')}
                            defaultValue={values.rol} 
                            placeholder="Ingrese rol"
                        />
                    </div>
                    <div class="form-group">
                    <label htmlFor="area">Area:</label>
                    <input 
                        type="text" 
                        className="form-control" 
                        id="area" 
                        onChange={handleChange('area')}
                        defaultValue={values.area} 
                        placeholder="Ingrese area"
                    />
                </div>
                    <div class="form-group">
                        <button 
                            className="btn btn-lg btn-primary btn-block" 
                            type="button"
                           
                            onClick={this._continue}
                        >Continuar
                        </button>
                    </div>
                    <div class="form-group">
                        <button 
                            className="btn btn-lg btn-secondary btn-block" 
                            type="button"
                           
                            onClick={this._back}
                        >Regresar
                        </button>
                    </div>
                
                </form>
                               
            </div>
        )
   }
}


export default FormPersonalDetails;