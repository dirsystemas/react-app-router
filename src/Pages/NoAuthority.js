import React, { Component } from 'react';

class NoAuthority extends Component {
   
    render() { 
        return ( 
            <div>
                <div className="container">
                    <div className='col-md-12'>
                        <div className='App-title'>
                            <h2  style={{color:'#eee'}}>Favor de autentificarse en el sistema.</h2>
                            <a href="/">Autentificarse</a>
                        </div>
                    </div>
                </div>
         </div>
         );
    }
}

export default NoAuthority;