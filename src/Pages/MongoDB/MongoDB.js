import React, { Component } from 'react';
import NavbarInverse from '../components/NavbarInverse';
import Footer from '../components/Footer';

class MongoDB extends Component {
  render() {
    return (
        <div>
            <NavbarInverse 
              itemMongoDB='disabled'
            />
        
            <main role="main" className="container" style={{marginTop:'150px'}}>
              <h1 className="mt-5">Temario para MongoDB</h1>
              <p className="lead">Pin a fixed-height footer to the bottom of the viewport in desktop browsers with this custom HTML and CSS. A fixed navbar has been added with <code>padding-top: 60px;</code> on the <code>body &gt; .container</code>.</p>
              
          </main>
            
            <Footer />
        </div>
    );
  }
}

export default MongoDB;
