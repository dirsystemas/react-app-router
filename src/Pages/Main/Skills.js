import React, { Component } from 'react';
import ItemBar from '../components/ItemBar';
import ButtonToogle from '../components/ButtonToogle';

class Skills extends Component{

    render(){
        return(
            <div className='container'>
                <div className='head-content d-flex align-items-baseline' >
                    <ButtonToogle 
                        id='btn-habilidades' 
                        dataTarget='#skills-content'
                        ariaControls='skills-content' 
                    />
                    <h3 className='sect-title' style={{marginLeft: '8px'}}>  Habilidades</h3>              
                </div>
                <div className='row collapse' id='skills-content'>

                    <div className='skills-col col-12 col-md-6'>    
                        <h4 className='sect-subtitle'>Desarrollo de Software</h4>
                        { this._renderCards(window._personal.skillsDevelop, 1) }
                    </div>  

                    <div className='skills-col col-12 col-md-6'>
                        <h4 className='sect-subtitle'>Otras</h4>
                        { this._renderCards(window._personal.skillsDevelop, 2) }        
                    </div>

                </div>
            </div> 
                
          
        )
    }

    _renderCards = (arr, type) => {
		return arr.map( (item, i) => {
            if(type == item.type) 
                return (
                    <ItemBar {...item} key= {i}/>
                );
		});
	};
}

export default Skills;