import React, { Component } from 'react';
import { Redirect } from 'react-router-dom'

import NavbarInverse from '../components/NavbarInverse';
import MainInfo from './MainInfo';
import Skills from './Skills';
import Portfolio from './Portfolio';


class Main extends Component {
    state = {
        redirectToReferrer: false
      }

    _onlogout() {
        this.setState({redirectToReferrer: true}) //return (<Redirect to={'/'}/>)alert('Paso en main')
    }  

  render() {
    if (this.state.redirectToReferrer) {
        return (<Redirect to={'/'}/>)
      } 
        
    return (
        <div>
            <NavbarInverse 
              itemMain='disabled'
            />
            <section className='main-info bg-grad' id='main-info'>
                <MainInfo />
            </section>
            <section className='sect skills' id='skills'>
                <Skills />
            </section>
            <section className='sect portfolio bg-grad-b' id='portfolio'>
                <Portfolio />
            </section>
            
        </div>
  );
  }
}

export default Main;