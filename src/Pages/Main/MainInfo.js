import React, { Component } from 'react';

class MainInfo extends Component{

    render(){
        const { 
            name,
            especialidad,
            habilidades,
            fecha_act,
            mobil,
            email,
            photo_url,
            direccion
        } = window._personal

        return(
            <div className='container'>
                <div className='row'>
                    <div className='col-12 col-md-5 col-xl-6 img-col d-flex'>
                        <div className='img-container d-flex justify-content-center'>
                            <img src={photo_url} style={{width:'120px', height:'150px'}} alt='Fotografia usuario'/>
                        </div>
                    </div>           
                    <div className='col-12 col-md-7 col-xl-6 txt-col'>
                        <div className='text-container'>
                        <h3 className='display-4 sect-title text-upercase'>{name}</h3>
                        <h4 className='display-4 text-upercase'>{especialidad}</h4>
                        <p className='lead'>{habilidades}</p>
                        <ul className='list-unstyled basic-info'>
                            <li><i className='fas fa-calendar-alt'></i> {fecha_act}</li>
                            <li><i className='fas fa-phone'></i> {mobil}</li>
                            <li><i className='fas fa-at'></i> {email}</li>
                            <li><i className='fas fa-map-marker-alt'></i> {direccion}</li>
                        </ul>
                        <ul className='list-inline social-nw'>
                            <li className='list-inline-item'><a href='#'><i className='fab fa-linkedin'></i></a></li>
                            <li className='list-inline-item'><a href='#'><i className='fab fa-twitter-square'></i></a></li>
                        </ul>
                        </div>
                    </div>
                </div>                   
            </div>       
                           
  
        )
    }

}

export default MainInfo;