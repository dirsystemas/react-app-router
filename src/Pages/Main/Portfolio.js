import React, { Component } from 'react';
import Card from '../components/Card';
import ButtonToogle from '../components/ButtonToogle';

class Portfolio extends Component{

    render(){
        return(
            <div>
                <div className='container'>
                    <div className='head-content d-flex align-items-baseline'>
                        <ButtonToogle 
                            id='btn-proyecto' 
                            dataTarget='#portfolio-content'
                            ariaControls='portfolio-content' 
                        />
                        <h3 className='sect-title' style={{marginLeft: '8px'}}>Proyectos destacados</h3>
                    </div>            
                </div>
                <div className='container-fluid collapse' id='portfolio-content'>
                    <div className='card-container d-flex flex-wrap justify-content-around align-items-center'>
                        { this._renderCards(window._personal.portfolio) }
                    </div>
                </div> 
            </div>    
         
        )
    }

    _renderCards = (arr) => {
		return arr.map( (item, i) => {
			return (
				<Card {...item} key= {i}/>
			);
		});
	};
}

export default Portfolio;