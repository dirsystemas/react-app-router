import React, { Component } from 'react';

class AppbarMult extends Component{

    render(){
        return(
            <header className="header-area fixed-top">
            <div className='header-multi'>
            <button tabIndex="0" type="button" className='btn-multi'>
                <div>
                    <svg viewBox="0 0 24 24" className='svg-multi'>
                        <path d="M3 18h18v-2H3v2zm0-5h18v-2H3v2zm0-7v2h18V6H3z"></path>
                    </svg>
                </div>
            </button>
            <h1 className='h1-multi'>
                {this.props.title}
            </h1>
        </div>
            </header>
        )
    }
}

export default AppbarMult;