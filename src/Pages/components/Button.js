import React from 'react';

export default function Button(props){
    return(
        <a 
            href={props.link} 
            className='btn btn-orange' 
        >{props.title}</a> 
    )
}