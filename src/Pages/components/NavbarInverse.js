import React from 'react';

export default function NavbarInverse(props){

    const _itemMain = `nav-link ${props.itemMain}`
    const _itemRecords = `nav-link ${props.itemRecords}`
    const _itemMongoDB = `nav-link ${props.itemMongoDB}`

    return(
        <header>
            <nav className="navbar navbar-expand-md navbar-dark fixed-top  bg-dark" >
                <span className="navbar-brand mb-0 h1">Sistema de Informacion</span>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarCollapse">
                    <ul className="nav ml-auto mr-auto mr-md-0" >
                        <li className="nav-item active">
                            <a className="nav-link" href="/">Inicio <span className="sr-only">(current)</span></a>
                        </li>
                        <li className="nav-item">
                            <a className={_itemMain} href="/main">Principal</a>
                        </li>
                        
                        <li className="nav-item">
                            <a className={_itemRecords} href="/records">Tabla Registros</a>
                        </li>
                        <li className="nav-item">
                            <a className='nav-link' href="/registro">Registro Usuarios</a>
                        </li>
                        <li className="nav-item">
                            <a className={_itemMongoDB} href="/mongodb">MongoDB</a>
                        </li>
                       
                    </ul>
                   
                </div>
            </nav>
        </header>
    )
}