import React, { Component } from 'react';
import Button from './Button';

class Card extends Component {

    render(){
        const { 
            icon, 
            title, 
            descripcion, 
            link 
        } = this.props

        return(
            <div className='card'>  
                <div className='card-body'>
                    <i className={icon}></i>
                    <h5 className='card-title'>{title}</h5>
                    <p className='card-text'>{descripcion}</p>
                    <Button link= {link} title='Ver mas ...'/>
                </div>
           </div>
        )
    }
}

export default Card;