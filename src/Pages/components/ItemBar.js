import React, { Component } from 'react';

class ItemBar extends Component {

    render(){
        const { 
            title, 
            widthPerc,
            width
        } = this.props

        
        return(
            <div className='skill-item'>
                <p className='text'>{title} </p>
                <div className='progress' style={{height: '20px'}}>
                    <div 
                        className='progress-bar progress-bar-striped bg-info' 
                        role='progressbar' 
                        style={{width:widthPerc, ariavaluenow:width, ariaValuemin:'0', ariavaluemax:'100'}}
                    >{ widthPerc}</div>
                </div>              
            </div>
        )
    }
}

export default ItemBar;