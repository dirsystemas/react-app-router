import React, { useState} from 'react';

export default function ButtonToogle(props){
    const [toogleOn, setToogleOn] = useState(true)
    return (
  
        <button 
            id={props.id}
            className='btn btn-sm btn-orange' 
            type='button'
            data-toggle='collapse' 
            data-target= {props.dataTarget}
            aria-expanded='false' 
            aria-controls={props.ariaControls}
            onClick={ () => setToogleOn( (toogleOn) => !toogleOn )}
            style={{
                margin: 10
            }}
            >
                { toogleOn ?  <i className='far fa-plus-square' ></i> :  <i className='far fa-minus-square' ></i>}
               
        </button>

        
    )

}