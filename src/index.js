import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

import Navigator from './Navigator';
import Reducers from './negocio/reducers'

const initialState = window.items
const Store = createStore(
    Reducers, 
    initialState,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    )
    Store.dispatch({
        type: 'INIT_ITEMS',
        items: window.items
      })


ReactDOM.render(
    <Provider store={Store}>
        <Navigator />
    </Provider>, 
    document.getElementById('root')
    );

